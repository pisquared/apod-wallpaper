#!/bin/bash
NASA_APOD_BASE_URL="https://apod.nasa.gov/apod"
IMAGE_URL=`curl -s $NASA_APOD_BASE_URL/astropix.html | grep -oh 'href="image/.*"' | grep -oh 'image/.*jpg' | head -1`
if [ -z "$IMAGE_URL" ]; then
  # gsettings set org.gnome.desktop.background picture-uri "file:///home/pi2/Pictures/wallpapers/blue_galaxy_wallpaper.png"
  #  xfconf-query --channel xfce4-desktop --property /backdrop/screen0/monitoreDP-1/workspace0/last-image --set /home/pi2/Pictures/wallpapers/blue_galaxy_wallpaper.png
  # xfconf-query --channel xfce4-desktop --property /backdrop/screen0/monitorHDMI-2/workspace0/last-image --set /home/pi2/Pictures/wallpapers/blue_galaxy_wallpaper.png
  feh --randomize --bg-fill /home/pi2/Pictures/wallpapers/*
else
  curl -s "$NASA_APOD_BASE_URL/$IMAGE_URL" > image.jpg
  # gsettings set org.gnome.desktop.background picture-uri "file:///$(pwd)/image.jpg"
  # xfconf-query --channel xfce4-desktop --property /backdrop/screen0/monitoreDP-1/workspace0/last-image --set "$(pwd)/image.jpg"
  # xfconf-query --channel xfce4-desktop --property /backdrop/screen0/monitorHDMI-2/workspace0/last-image --set "$(pwd)/image.jpg"
  feh --bg-fill "$(pwd)/image.jpg"
fi
